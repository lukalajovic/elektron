import elektrika.Elektron;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.Timer;

@SuppressWarnings("serial")
public class Platno extends JPanel implements ActionListener {
	private Vector<Elektron> elektroni;
	public Timer cajt = new Timer(10, this);	
	public Platno(){
		super();
		this.elektroni= new Vector<Elektron>();
		setBackground(Color.WHITE);	
	}
	public void dodajelektron(Elektron elektron) {
		this.elektroni.add(elektron);
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		for (Elektron elektron : this.elektroni) {
			g.setColor(Color.BLUE);
			elektron.narisi(g);
		}

		cajt.start();
	}
	
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(800, 800);}
	


	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==cajt){
			
		
		for(Elektron elektron:this.elektroni){
			elektron.x+=1;
			if (elektron.x>500){
				elektron.x=0;
			}
		}
		repaint();}
		else{
			cajt.stop();
		}

}

	
	
	
}

