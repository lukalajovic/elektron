import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JFrame;
import javax.swing.JButton;

@SuppressWarnings("serial")
public class GlavnoOkno extends JFrame {
	
	public Platno platno;
	private JButton konec;
//    GridLayout mreza = new GridLayout(0,2);
    
	public Platno getPlatno() {
		return platno;
	}

	public GlavnoOkno() {
		super("Nabiti Delci");
//		this.setLayout(mreza);
		
		this.platno = new Platno();
		this.add(this.platno);
		setLayout(new GridBagLayout());
		
		konec=new JButton("konec");
		this.add(konec);
		konec.addActionListener(platno);
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridy = 0;
		c.gridx = 0;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.anchor = GridBagConstraints.CENTER;
		add(this.platno, c);
		c = new GridBagConstraints();
		c.gridy = 1;
		c.gridx = 2;
		add(konec, c);		
	}

}
