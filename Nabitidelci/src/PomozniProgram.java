import java.util.Timer;
import java.util.TimerTask;

public class PomozniProgram extends TimerTask {
	public int a;

	public PomozniProgram(int a) {
		this.a = a;
	}

	public void run() {
		a += 1;
		System.out.println(a);

	}

	public static void main(String args[]) {

		TimerTask timerTask = new PomozniProgram(7);
		Timer timer = new Timer(true);
		timer.scheduleAtFixedRate(timerTask, 0, 1000);
		// cancel after sometime
		try {
			Thread.sleep(20000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		timer.cancel();
	}
}
