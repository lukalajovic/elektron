import elektrika.*;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.Vector;

import javax.swing.JPanel;



@SuppressWarnings("serial")
public class Platno extends JPanel {
	public Vector<Elektron> elektroni;
	public Platno() {
		super();
		this.elektroni = new Vector<Elektron>();
		setBackground(Color.WHITE);
	}

	public void dodajLik(Elektron elektron) {
		this.elektroni.add(elektron);
	}
	
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(700, 700);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.RED);
		for (Elektron elektron : this.elektroni) {
			elektron.narisi(g);
		}
	}

}